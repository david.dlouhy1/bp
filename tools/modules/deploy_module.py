"""
The module is focused on the deployment of images.
"""
import os
import subprocess
import shutil
from discordwebhook import Discord

directory = "deploy_workspace"

def run_bash_command(bash_command):
    """
    Run bash command from python

    Arguments:
        bash_command: Command as array

    Returns:
        Status code and command output
    """
    process = subprocess.Popen(bash_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    _, error = process.communicate()
    return (process.returncode, error.decode("utf-8"))

def deploy_rpi_image_to_sd_card(sd_card, scp_remote_server, scp_user, scp_remote_image_path, scp_image_name):
    """
    Deploy the image for the Raspberry Pi. It's downloads the image from the server and flash the image to the sd card.

    Arguments:
        sd_card: The card on which the deployment is performed
        scp_remote_server: Server for SCP
        scp_user: User for SCP
        scp_remote_image_path: Remote path for SCP
        scp_image_name: Image for flashing

    Returns:
        Result of flashing
    """
    result=(None, None)
    if sd_card == None or scp_remote_server == None:
        return None

    script_dir = os.getcwd()

    if not os.path.exists(directory):
        os.mkdir(directory)

    os.chdir(script_dir+"/"+directory)

    scp_download_image = ["scp", scp_user+"@"+scp_remote_server+":"+scp_remote_image_path+scp_image_name, "."]
    scp_ret_code, scp_error = (run_bash_command(scp_download_image))
    if(scp_ret_code == 0):
        dd_flash_image = ["sudo", "dd",'if='+scp_image_name, 'of='+sd_card, 'bs=1M']
        dd_ret_code, dd_error = run_bash_command(dd_flash_image)
        if(dd_ret_code == 0):
            result=(True, None)
        else:
            result=(False, dd_error)
    else:
        result=(False, scp_error)

    os.chdir(script_dir)
    shutil.rmtree(directory, ignore_errors=True)

    return result

def notify_to_discord(url, message):
    """
    Sends notification to Discord channel

    Arguments:
        url: Discord webhook url for bot
        message: Text message sended by bot

    """
    discord = Discord(url=url)
    discord.post(content=message)
    pass
