import pymongo
import json
import os
from pathlib import Path
from datetime import datetime
from bson.objectid import ObjectId

class MongoDbConnection:
    def __init__(self):
        with open(os.path.dirname(Path(__file__))+'/mongo_module.json', 'r') as f:
            data = json.load(f)
        self.mongo_db_client = pymongo.MongoClient(data['mongo_url'])
        self.mongo_db_database = self.mongo_db_client.get_database(data['database_name'])
        self.mongo_db_collection = self.mongo_db_database.get_collection(data['collection_name'])
    #Funkce na získání volného id
    def init_build_mongo(self):
        res = self.mongo_db_collection.insert_one({"init":True}).inserted_id
        return res
    #Fukce na zápis buildu
    def save_build_to_mongo(self, json_data, data_id):
        #pokud je uvedeno, tak proveď update
        if(data_id):
            self.mongo_db_collection.update_one({
                '_id':ObjectId(data_id)
                },
                {
                    "$set": json_data
                } 
            )
        else:
            #pokud id není uvedeno
            self.mongo_db_collection.insert_one(json_data)

        return
    #Funcke na update build -> edituje deploy informace
    def save_deploy_to_mongo(self, cicd_pair_id, ci_pipeline_iid, json_data):

        cicd_build_item_id = self.mongo_db_collection.find({
            'cicd_pair_id':cicd_pair_id,
            'ci_pipeline_iid': ci_pipeline_iid
        }).sort("insert_dt",-1).limit(1)[0]['_id']

        self.mongo_db_collection.update_one({
            '_id':cicd_build_item_id
            },
            {
                "$set": json_data
            } 
        )
        return


def mongo_connect():
    return MongoDbConnection()