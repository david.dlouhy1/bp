#!/bin/bash
filesize=$(wc -c $1 | awk '{print $1}')

mb=$(bc <<<"scale=6; $filesize / 1048576")
echo "IMAGE_SIZE=$mb" #MB