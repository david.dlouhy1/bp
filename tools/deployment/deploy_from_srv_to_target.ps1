﻿<#
  .SYNOPSIS
  Skript pro nasazení image ze serveru

  .DESCRIPTION
  Skript vyžaduje PowerShell a nainstalovaný nástroj dd. Primárně je určen pro Windows, ale díky tomu, že PowerShell je multiplatformní, tak se dá využít například i na Linuxu. Předpokládá se, že uživatel má nistalované dd, scp a má nahraný SSH klíč pro přístup na uživtele na serveru.
  
  Skript provádí následující sadu úkolů:
    - Připojí se na vzdálený server
    - Stáhne požadovaný soubor do work foderu
    - Odhlásí se
    - Provede flash na externí disk

  Předpokládá se, že uživatel má nistalované dd, scp a má nahraný SSH klíč pro přístup na uživtele na serveru.

  .PARAMETER s
  Říká jaký server se má využít. Od uživatele je vyžadována IP nebo hostname.

  .PARAMETER u
  Volba uživatele na kteŕeho se přihlašujeme na server.

  .PARAMETER i
  Název image včetně absolutní cesty na serveru.

  .PARAMETER t
  Volby typu nasazení
   - rpi-sd - použijeme pokud chceme abychom nahráli ze serveru image přímo do sd karty, která je určená pro Raspberry Pi.

  .PARAMETER d
  Písmeno disku, kam se má provést deploy.
  Povinné pouze pro:
   - rpi-sd 

  .INPUTS
  Nic

  .OUTPUTS
  Výstupem je systémová notifikace včeteně výpisu do konzole.

  .EXAMPLE
  PS> .\deploy_from_srv_to_target.ps1 -s 192.168.0.13 -u david -i /home/david/gitlab-ci-cd/bp/yocto-images/test.txt -t rpi-sd

#>
param (
    [String]$s,
    [String]$u,
    [String]$i, 
    [String]$t
)

[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") 

if (($null -eq $s) -or ("" -eq $s)) {
    $s = read-host -Prompt "Server IP or hostname?" 
}

if (($null -eq $u) -or ("" -eq $u)) {
    $u = read-host -Prompt "User?" 
}

if (($null -eq $i) -or ("" -eq $i)) {
    $i = read-host -Prompt "Image with path?" 
}

if (($null -eq $t) -or ("" -eq $t)) {
    $t = read-host -Prompt "Type?" 
}
#-------------------------------------------------------------------------------------------------------------
#Nastavení proměnných
$MESSAGE_SUCCESS = 'Image byl úspěnšně nasazen.'
$MESSAGE_WARNING = 'Zdá se, že nepoužíváte korektní syntaxi. Zkontrolujte správné použití skriptu.'
$MESSAGE_ERROR = 'Došlo k neočekávané chybě při běhu skriptu. Zkontrolujte správné použití skriptu.'

$MESSAGE_HEAD_SUCCESS = 'Úspěšně provedeno'
$MESSAGE_HEAD_WARNING = 'Nesprávné použití skriptu'
$MESSAGE_HEAD_ERROR = 'Skript skončil s chybou'

$ICON_SUCCESS = [System.Windows.Forms.ToolTipIcon]::Info
$ICON_WARNING = [System.Windows.Forms.ToolTipIcon]::Warning
$ICON_ERROR = [System.Windows.Forms.ToolTipIcon]::Error

$COLOR_SUCCESS='Green'
$COLOR_WARNING='Yellow'
$COLOR_ERROR='Red'

$success=1
$notification_message=$MESSAGE_WARNING
$notification_icon=$ICON_WARNING
$notification_color=$COLOR_WARNING
$notifiaction_head=$MESSAGE_HEAD_WARNING

$WORK_DIR="running_deployment"

Add-Type -AssemblyName System.Windows.Forms
$global:notification_item = New-Object System.Windows.Forms.NotifyIcon
$path = (Get-Process -id $pid).Path

#-------------------------------------------------------------------------------------------------------------
#Provedení skriptu
if(("" -eq $s) -or ("" -eq $u) -or ("" -eq $i) -or ("" -eq $t)){
    $success=0
}
else {
    New-Item -Path "." -Name $WORK_DIR -ItemType "directory" | Out-Null
    Set-Location -Path "$WORK_DIR"
    #1. Připojí se na vzdálený server
    # úspěch je když se připojí
    # neúspěch je, kdiž se nepřipojí, nebo když se neprovede úspěšně autrizace
    #2. Stáhne požadovaný soubor do work foderu
    # úspěch je když se stáhne
    # neúspěch, když to ten soubor nenajde
    #3. Odhlásí se
    try{
        scp $u'@'$s':'$i '.'
        if(!$?){
            throw
        }
    }
    catch{
        $success=-1
    }
    if ($success -ne -1) {
        switch ($t) {
            "rpi-sd" { 
                #flase
                Write-Host "asdsad"
                if("" -eq $d){
                    $success=0
                }
                else {
                    try {
                        dd if=\6.rpi-sdimg of=g:
                    }
                    catch {
                        
                    }
                }
            }
            Default {$success=0}
        }
    }
    #4. Provede flash na externí disk
    #úspěch je, kdež najde systémovou jendotku, úspěšně ji promaže a nahraje co má
    Set-Location ".."   
    Remove-Item -Path $WORK_DIR -Force -Recurse
}
#-------------------------------------------------------------------------------------------------------------
#Vyhodnocení výsledků a nastavení výpisu

if($success -eq 1){
    $notification_message=$MESSAGE_SUCCESS
    $notification_icon=$ICON_SUCCESS
    $notification_color=$COLOR_SUCCESS
    $notifiaction_head=$MESSAGE_HEAD_SUCCESS
}

if($success -eq -1){
    $notification_message=$MESSAGE_ERROR
    $notification_icon=$ICON_ERROR
    $notification_color=$COLOR_ERROR
    $notifiaction_head=$MESSAGE_HEAD_ERROR
}

$notification_item.Icon = [System.Drawing.Icon]::ExtractAssociatedIcon($path)
$notification_item.BalloonTipText = $notification_message
$notification_item.BalloonTipIcon = $notification_icon
$notification_item.BalloonTipTitle = $notifiaction_head
$notification_item.Visible = $true


#-------------------------------------------------------------------------------------------------------------
#Výpis a upozornění
Write-Host $notification_message -ForegroundColor $notification_color
$notification_item.ShowBalloonTip(20000)
#-------------------------------------------------------------------------------------------------------------