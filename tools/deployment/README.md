# Deployment
Zde se nacházejí nástroje pro deployment image.

## Skript `image_deploy.py`<br>
<hr>
Skirpt napsaný v jazyce Python 3.6 je určen pro automatizaci CI/CD. Obstarává nasazování vytvořených images.
<br><br>

### **Usecase**

Následuje seznam typů nasazení, které jsou podporovány tímto skriptem. Podrobné použití je k dispozici v dokumentačním komentáři jednotlivých součástí skirptu.<br><br>
**Raspberry Pi**<br>
Skript se vzáleně připojí na build server odkud si stáhne image. Náshledně prohběhne flash toho image na sd kartu. Skript je také napojen na info kanál v programu Discord.
<br><br>
Před prvním použitím je třeba upravit proměnné ve skriptu a zada své vlastní hodnoty - viz komentáře ve skriptu.<br> Pozor při prvním spuštění se mě to zeptalo na fingerprint.
<br>
Nutno si dát pozor na aby ssh fungovalo skrze klíč!<br>
U dd je nudo přidat uživatele do suers <br>

`python image_deploy.py --source "192.168.0.13" --user "david" --deptype "rpi-sd" --sd_card "/dev/sdb" --rspath "/home/david/gitlab-ci-cd/bp/yocto-images/" --image "example-image-raspberrypi3.rpi-sdimg"`

## Skript `deploy_from_srv_to_target.ps1`<br>
<hr>

**Depracated** - ukázalo se, že je lepší využít Linuxový stroj a Python<br><br>
Skript vyžaduje PowerShell a nainstalovaný nástroj dd. Primárně je určen pro Windows, ale díky tomu, že PowerShell je multiplatformní, tak se dá využít například i na Linuxu. <br>
Předpokládá se, že uživatel má nistalované dd, scp a má nahraný SSH klíč pro přístup na uživtele na serveru.<br>
### Užité parametry pro skript:<br>
`-h` zobrazí nápovědu<br>
`-i` název image<br>
`-t` typ cíle<br>
`-s` ip adressa serveru, kde je image
`-u` uźivatel, který se připojuje na server
### Skript provádí následující sadu úkolů:<br>
1. Připojí se na vzdálený server
2. Stáhne požadovaný soubor do work foderu
3. Odhlásí se
4. Provede flash na externí disk
