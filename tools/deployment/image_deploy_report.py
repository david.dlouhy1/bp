import argparse
import sys
import os
from pathlib import Path
sys.path.append(os.path.join(os.path.dirname(Path(__file__).parent),'modules'))
from datetime import datetime
import mongo_module


def send_report(args):
    json_data={
            'insert_deploy_dt': datetime.today().replace(microsecond=0),
            'deploy_time': int(args.deploy_time),
            'deploy_device': args.deploy_device,
            'deployed_image': args.deployed_image,
            'size_of_deyploed_image':float(args.size_of_deyploed_image),
            'deploy_success':args.deploy_success == 'True'
        }
    db_con.save_deploy_to_mongo(int(args.cicd_pair_id), int(args.ci_pipeline_iid), json_data)
    return

db_con = mongo_module.mongo_connect()
parser = argparse.ArgumentParser(prog='image_deploy_report', description='TODO', epilog='example: \n  TODO"\n ',formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('--cicd_pair_id', help='TODO', required=True)
parser.add_argument('--ci_pipeline_iid', help='TODO', required=True)
parser.add_argument('--deploy_time', help='TODO', required=True)
parser.add_argument('--deploy_device', help='TODO', required=True)
parser.add_argument('--deployed_image', help='TODO', required=True)
parser.add_argument('--size_of_deyploed_image', help='TODO', required=True)
parser.add_argument('--deploy_success', help='TODO', required=True)


args = parser.parse_args()

send_report(args)

# python3 image_deploy_report.py --cicd_pair_id 1 --ci_pipeline_iid 23 --deploy_time 39 --deploy_device '192.168.0.12' --deployed_image 'example-image.sdimg' --size_of_deyploed_image 105 --deploy_success True