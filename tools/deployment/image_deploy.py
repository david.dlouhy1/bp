import argparse
import sys
import os
from pathlib import Path
sys.path.append(os.path.join(os.path.dirname(Path(__file__).parent),'modules'))
import deploy_module

#Private
#You must replace <VALUE> with your values and uncommnet all private variables! Then you can commned exit statemnet
#------------------
#sd_card = "<DEVICE>"
#discord_webhook = "<WEBHOOKURL>"
exit() #commnet this!
#------------------

def run_depoly_by_type(args):
    """
    Select and run deployment by inserted type

    Arguments:
        args: CLI arguments

    Returns:
        Output of deployment or None
    """
    if args.deptype =='rpi-sd':
        return deploy_module.deploy_rpi_image_to_sd_card(args.sd_card, args.source, args.user, args.rspath, args.image)
    return (''+args.deptype, (None, None))

def run_command():
    """
    Organistaion function - run this
    """
    print("Deployment is in progress...")
    result, output=run_depoly_by_type(args)
    output_message = "Wrong use of script."
    done_mess="FALSE"
    if result == True:
        output_message = "Succesfully deployed!"
        done_mess="DONE"
    elif result == False:
        output_message = "Runtime error!\n "+output
    print(output_message)
    print(done_mess)
    deploy_module.notify_to_discord(discord_webhook,output_message)
    pass

parser = argparse.ArgumentParser(prog='image_deploy', description='Do deployment of Yocto restult to target.', epilog='example: \n  python image_deploy.py --source "192.168.0.13" --user "david" --deptype "rpi-sd" --sd_card "/dev/sdb" --rspath "/home/david/gitlab-ci-cd/bp/yocto-images/" --image "example-image-raspberrypi3.rpi-sdimg"\n ',formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('--deptype', type=str, help='deploy type [rpi-sd, ...]', required=True)
parser.add_argument('--image', type=str, help='name of image [example.rpi-sdimg, ...]', required=True)
parser.add_argument('--source', type=str, help='source device where image is [192.168.0.1, hostname, ...]', required=True) 
parser.add_argument('--rspath', help='remote source path [/home/user/data, ...]')
parser.add_argument('--user', help='user of remote source [192.168.0.1, hostname, ...]')    
parser.add_argument('--sd_card', help='the card on which the deployment is performed')

args = parser.parse_args()

run_command()
