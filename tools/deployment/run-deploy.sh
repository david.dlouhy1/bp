#!/bin/bash
error_state=0
echo True > deploy_success.txt
date
start=`date +%s`
ssh david@192.168.0.13 -i ~/.ssh/id_rsa "cd git/bp/tools/deployment; sh file_size_mb.sh ${DIMAGEPATH}${DIMAGE}" | tee deploy_output.txt
SIZE_OF_DEPLOYED_IMAGE=$( cat deploy_output.txt | grep IMAGE_SIZE | tr "=" "\n" )
SIZE_OF_DEPLOYED_IMAGE=$( echo ${SIZE_OF_DEPLOYED_IMAGE}  | cut -d " " -f 2)
ssh david@192.168.0.12 -i ~/.ssh/id_rsa "cd gitlab-ci-cd/deployment; python image_deploy.py --source \"192.168.0.13\" --user \"david\" --deptype \"rpi-sd\" --sd_card \"/dev/sdb\" --rspath \"${DIMAGEPATH}\" --image \"${DIMAGE}\"" | tee deploy_output.txt
date
end=`date +%s`
runtime=$((end-start))



DONE=$( tail -n 1 deploy_output.txt )
if [ "$DONE" != "DONE" ]
then
error_state=1
echo False > deploy_success.txt
pwd
fi
success=$( cat  deploy_success.txt )

rm deploy_success.txt
rm deploy_output.txt

python3 image_deploy_report.py --cicd_pair_id ${CICD_PAIR_ID} --ci_pipeline_iid ${CI_PIPELINE_IID} --deploy_time ${runtime} --deploy_device '192.168.0.12' --deployed_image ${DIMAGE} --size_of_deyploed_image "${SIZE_OF_DEPLOYED_IMAGE}" --deploy_success ${success}


if [ $error_state -ne 0 ]
then
exit 1
fi
