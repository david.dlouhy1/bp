#!/bin/bash
CONTENT_I=""
while getopts 'i:h' opt; do
  case "$opt" in
    i)
      CONTENT_I="$OPTARG"
      ;;
   
    ?|h)
      echo "Usage: $(basename $0) [-i arg] [-h]"
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

echo $CONTENT_I
#pokud se rovná toto tak zbuildi toto