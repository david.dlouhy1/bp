# Tools
Zde se nacházejí informace ohlendě virtuálních strojů.

## start.sh - bude upřesněno 
Jedná se o script určený k interaktivnímu vývoji pod projektem Yocto. Připraví a připojí se interkativně k vývojovému kontejneru, kde můžeme následně vést vývoj. Pro spuštění je třeba být v tomto adresáři a zadat příkaz `sh start_contianer.sh`. Nápověda lze zobrazit dle standardního přepínace `-h`.

## build_docker_images.sh
Skript, který sestraví z příslušných Dockerfile potřebné images pro Docker kontejnery.
<br><br>
Pozor při sestavování je nutné dodat ssh kontejneru privátní ssh klíč - soubor s názvem `id_rsa` a `authorized_keys` (známé hosty). Klíč a hosty umístěte do adresáře, kde je Dockerfile. Cesta je vidět ve skriptu.

## clean.sh - bude upřesněno 
Skript určený pro kompetní vyčištění prostředí. Možnost výběru co cheme smaza. Image, kontejner...

## auto_build.sh
Skript, který sestavý požadovaný image automaticky. Přebírá parametr `-i IDENTIFIKATOR`. Ten říká, co má Yocto sestavit. Přehled aktuálně dostupných distribucí nalzenete ve výchozím adresáři. 


## Start statsserver
NUTNO doinstalovat docker compose!

## build/build_docs.sh
Pomocí SSH se připojí na testvací server a zde vygeneruje dokumentaci určenou k nasazení.

##