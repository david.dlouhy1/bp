#!/bin/bash
rb_image="example-image"
rb_build="rpi-build-2"
cicdid=$1
export CICDID=$(echo $cicdid)
echo "RB_BUILD=${rb_build}"
echo "RB_IMAGE=${rb_image}"
echo "CICDID=${CICDID}"
cd ../../workspace 
echo ${CICDID} > sources/meta-hubshuffle/recipes-stats/statssender/files/cicdid.txt
OLD_PWD=$( pwd )
. ./sources/poky/oe-init-build-env ${rb_build}
bitbake ${rb_image}
if [ $? -eq 0 ]; then
    echo "" > ${OLD_PWD}/sources/meta-hubshuffle/recipes-stats/statssender/files/cicdid.txt
    echo "DONE"
else
    echo "" > ${OLD_PWD}/sources/meta-hubshuffle/recipes-stats/statssender/files/cicdid.txt
   echo "FAIL"
   exit 1
fi