import argparse
import sys
import os
from datetime import datetime
from pathlib import Path
sys.path.append(os.path.join(os.path.dirname(Path(__file__).parent),'modules'))
import mongo_module

def run_command():
    db_con.save_build_to_mongo({
        "cicd_pair_id":int(args.cicd_pair_id),
        "ci_pipeline_iid":int(args.ci_pipeline_iid),
        "rb_build":args.rb_build,
        "rb_image":args.rb_image,
        "insert_dt":datetime.today().replace(microsecond=0),
        "build_success":(args.success_build == 'True'),
        "build_time":int(args.build_time)
    }, args.cicdid)
    pass

parser = argparse.ArgumentParser(prog='image_build_report', description='Reports ', epilog='example: \n  TODO"\n ',formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('--cicd_pair_id', help='Custom ID for CI/CD pairs', required=True)
parser.add_argument('--ci_pipeline_iid', help='ID of project pipeline', required=True)
parser.add_argument('--rb_build', help='Name of build', required=True)
parser.add_argument('--rb_image', help='Builded image', required=True)
parser.add_argument('--success_build', help='Resutl state of build stage', required=True)
parser.add_argument('--build_time', help='Time of build', required=True)
parser.add_argument('--cicdid', help='ID in MongoDB for CICD')

args = parser.parse_args()
db_con = mongo_module.mongo_connect()
run_command()