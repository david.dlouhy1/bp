#!/bin/bash
#Run this like -> sh run-build.sh rpi-build-2_example-image.sh
error_state=0
echo True > buid_success.txt
cp /yocto/tools/modules/mongo_module.json .
CICDID=$( python3 get_id_for_cicd.py )
echo $CICDID
date
start=`date +%s`
ssh david@192.168.0.13 -i ~/.ssh/id_rsa "cd git/bp; docker container run --rm -v \"${CPWD}\":/opt/yocto --name hubshuffle-yocto --volume \"${CPWD}/home\":/home/yocto hubshuffle/yocto:1.2 bash -c \"groupadd -g 7777 yocto && useradd --password U6aMy0wojraho --shell /bin/bash -u ${CUID} -g 7777 yocto && usermod -aG sudo yocto && usermod -aG users yocto && cd /opt/yocto && su - yocto && sudo -u yocto bash -c 'cd /opt/yocto/tools/build/ && sh ${BUILD_SCRIPT} ${CICDID}'\"" | tee build_output.txt
date
end=`date +%s`
runtime=$((end-start))
RB_BUILD=$( cat build_output.txt | grep RB_BUILD | tr "=" "\n" )
RB_IMAGE=$( cat build_output.txt | grep RB_IMAGE | tr "=" "\n" )
RB_BUILD=$( echo ${RB_BUILD}  | cut -d " " -f 2)
RB_IMAGE=$( echo ${RB_IMAGE}  | cut -d " " -f 2)

DONE=$( tail -n 1 build_output.txt )

if [ "$DONE" != "DONE" ]
then
error_state=1
echo False > buid_success.txt
pwd
fi
success=$( cat  buid_success.txt )

rm mongo_module.json
rm build_output.txt
rm buid_success.txt

python3 image_build_report.py --cicd_pair_id ${CICD_PAIR_ID} --ci_pipeline_iid ${CI_PIPELINE_IID} --success_build ${success} --build_time ${runtime} --rb_build ${RB_BUILD} --rb_image ${RB_IMAGE} --cicdid ${CICDID}


if [ $error_state -ne 0 ]
then
exit 1
fi