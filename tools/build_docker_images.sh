#!/bin/sh

#YOCTO
YOCTO_IMAGE="yocto:c-1-0-0"
WORKBENCH_DIR=workbench
mkdir $WORKBENCH_DIR
cd $WORKBENCH_DIR
cp ../../docker/yocto-machine/Dockerfile ./Dockerfile
docker build -t $YOCTO_IMAGE  .
cd ..
rm $WORKBENCH_DIR -rf

#SSH BUILD
YOCTO_IMAGE="yocto-ssh:1.0"
WORKBENCH_DIR=workbench
mkdir $WORKBENCH_DIR
cd $WORKBENCH_DIR
cp ../../docker/ssh-machine/Dockerfile ./Dockerfile
cp ../../docker/ssh-machine/known_hosts ./known_hosts
cp ../../docker/ssh-machine/id_rsa ./id_rsa
docker build -t $YOCTO_IMAGE  .
cd ..
rm $WORKBENCH_DIR -rf

#HUBSHUFFLE/YOCTO:1.2
YOCTO_IMAGE="hubshuffle/yocto:1.2"
WORKBENCH_DIR=workbench
mkdir $WORKBENCH_DIR
cd $WORKBENCH_DIR
cp ../../docker/ssh-machine/Dockerfile ./Dockerfile
docker build -t $YOCTO_IMAGE  .
cd ..
rm $WORKBENCH_DIR -rf

#sphinx:1.0
YOCTO_IMAGE="sphinx:1.0"
WORKBENCH_DIR=workbench
mkdir $WORKBENCH_DIR
cd $WORKBENCH_DIR
cp ../../docker/docs-machine/Dockerfile ./Dockerfile
docker build -t $YOCTO_IMAGE  .
cd ..
rm $WORKBENCH_DIR -rf