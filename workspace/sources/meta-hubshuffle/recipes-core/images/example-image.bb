# Base this image on core-image-base
require recipes-core/images/core-image-base.bb

# Only allow machines wich start "rpi"
COMPATIBLE_MACHINE = "^rpi$"

# Install curl
# Install my recepy called statssender
# Install systemd
# Install systemd-analyze
IMAGE_INSTALL_append = " curl statssender systemd systemd-analyze fastset tulwelcome"