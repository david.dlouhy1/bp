SUMMARY = "Appends script for setup fastboot."
LICENSE = "CLOSED"
SRC_URI += "file://fastboot_script.conf"
SRC_URI += "file://fastboot_script.sh"

RDEPENDS_${PN} += "bash"

do_install(){
    install -m 0755 ${WORKDIR}/fastboot_script.conf ${D}/
    install -m 0755 ${WORKDIR}/fastboot_script.sh ${D}/
}

FILES_${PN} += "/fastboot_script.conf"
FILES_${PN} += "/fastboot_script.sh"