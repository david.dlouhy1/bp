#!/bin/bash

. ./fastboot_script.conf

DISABLE="[SET]"
ROLLBACK="[REC]"

do_disable_hciuart () {
   echo ${DISABLE}' hciuart'
   systemctl disable hciuart
}
do_disable_systemd_resolved () {
   echo ${DISABLE}' systemd-resolved'
   systemctl disable systemd-resolved.service
}
do_disable_kmod_static_nodes () {
   echo ${DISABLE}' kmod-static-nodes'
   	systemctl disable kmod-static-nodes.service
    systemctl mask kmod-static-nodes.service
}
do_disable_dev_mqueue_mount () {
   echo ${DISABLE}' dev-mqueue.mount'
   systemctl mask dev-mqueue.mount
}
do_disable_bluetooth () {
   echo ${DISABLE}' bluetooth'
   systemctl stop bluetooth.service
   systemctl disable bluetooth.service
   systemctl mask bluetooth.service
}
do_disable_ofono () {
   echo ${DISABLE}' ofono'
   systemctl disable ofono.service
}
do_disable_avahi_daemon () {
   echo ${DISABLE}' avahi.daemon'
   systemctl stop avahi-daemon.service
   systemctl stop avahi-daemon.socket
   systemctl stop avahi-daemon.service
   systemctl disable avahi-daemon.socket
   systemctl disable avahi-daemon.service
   systemctl mask avahi-daemon.socket
   systemctl mask avahi-daemon.service
}
do_disable_systemd_journal_flush () {
   echo ${DISABLE}' systemd-journal-flush.service'
   systemctl disable systemd-journal-flush.service
   systemctl mask systemd-journal-flush.service
}
do_disable_systemd_random_seed () {
   echo ${DISABLE}' systemd-random-seed.service'
   systemctl stop systemd-random-seed.service
   systemctl disable systemd-random-seed.service
   systemctl mask systemd-random-seed.service
}
do_disable_systemd_timesyncd () {
   echo ${DISABLE}' systemd-timesyncd.service'
   systemctl disable systemd-timesyncd.service
}
do_disable_systemd_networkd () {
   echo ${DISABLE}' systemd-networkd.service'
   systemctl disable systemd-networkd.service
   systemctl mask systemd-networkd.service
}
do_disable_systemd_rfkill () {
   echo ${DISABLE}' systemd-rfkill'
   systemctl stop systemd-rfkill.service
   systemctl stop systemd-rfkill.socket
   systemctl stop systemd-rfkill.service
   systemctl disable systemd-rfkill.socket
   systemctl disable systemd-rfkill.service
   systemctl mask systemd-rfkill.socket
   systemctl mask systemd-rfkill.service
}
do_enable_hciuart () {
   echo ${ROLLBACK}' hciuart'
   systemctl enable hciuart
}
do_enable_systemd_resolved () {
   echo ${ROLLBACK}' systemd-resolved'
   systemctl enable systemd-resolved.service
}
do_enable_kmod_static_nodes () {
   echo ${ROLLBACK}' kmod-static-nodes'
   systemctl unmask kmod-static-nodes.service
   systemctl enable kmod-static-nodes.service
}
do_enable_dev_mqueue_mount () {
   echo ${ROLLBACK}' dev-mqueue.mount'
   systemctl unmask dev-mqueue.mount
}
do_enable_bluetooth () {
   echo ${ROLLBACK}' bluetooth'
   systemctl unmask bluetooth.service
   systemctl enable bluetooth.service
   systemctl start bluetooth.service
}
do_enable_ofono () {
   echo ${ROLLBACK}' ofono'
   systemctl enable ofono.service
}
do_enable_avahi_daemon () {
   echo ${ROLLBACK}' avahi.daemon'
   systemctl unmask avahi-daemon.socket
   systemctl unmask avahi-daemon.service
   systemctl enable avahi-daemon.socket
   systemctl enable avahi-daemon.service
   systemctl start avahi-daemno.socket
   systemctl start avahi-daemno.service
   systemctl start avahi-daemon.socket
}
do_enable_systemd_journal_flush () {
   echo ${ROLLBACK}' systemd-journal-flush.service'
   systemctl unmask systemd-journal-flush.service
   systemctl enable systemd-journal-flush.service
}
do_enable_systemd_random_seed () {
   echo ${ROLLBACK}' systemd-random-seed.service'
   systemctl unmask systemd-random-seed.service
   systemctl enable systemd-random-seed.service
   systemctl start systemd-random-seed.service
}
do_enable_systemd_timesyncd () {
   echo ${ROLLBACK}' systemd-timesyncd.service'
   systemctl enable systemd-timesyncd.service
}
do_enable_systemd_networkd () {
   echo ${ROLLBACK}' systemd-networkd.service'
   systemctl unmask systemd-networkd.service
   systemctl enable systemd-networkd.service
   systemctl start systemd-networkd.service
   sleep 4
}
do_enable_systemd_rfkill () {
   echo ${ROLLBACK}' systemd-rfkill'
   systemctl unmask systemd-rfkill.service
   systemctl unmask systemd-rfkill.socket
   systemctl enable systemd-rfkill.socket
   systemctl enable systemd-rfkill.service
   systemctl start systemd-rfkill.service
   systemctl start systemd-rfkill.socket
}

if [ ${DISABLE_HCIUART} == 1 ] 
then
do_disable_hciuart
else
    if [[ "${DISABLE_HCIUART}" == "R" ]]
    then
    do_enable_hciuart
    fi
fi

if [ ${DISABLE_SYSTEMD_RESOLVED} == 1 ]
then
do_disable_systemd_resolved
else
    if [[ "${DISABLE_SYSTEMD_RESOLVED}" == "R" ]]
    then
    do_enable_systemd_resolved
    fi
fi

if [ ${DISABLE_KMOD_STATIC_NODES} == 1 ]
then
do_disable_kmod_static_nodes
else
    if [[ "${DISABLE_KMOD_STATIC_NODES}" == "R" ]]
    then
    do_enable_kmod_static_nodes
    fi
fi

if [ ${DISABLE_DEV_MQUEUE_MOUNT} == 1 ]
then
do_disable_dev_mqueue_mount
else
    if [[ "${DISABLE_DEV_MQUEUE_MOUNT}" == "R" ]]
    then
    do_enable_dev_mqueue_mount
    fi
fi

if [ ${DISABLE_BLUETOOTH} == 1 ]
then
do_disable_bluetooth
else
    if [[ "${DISABLE_BLUETOOTH}" == "R" ]]
    then
    do_enable_bluetooth
    fi
fi

if [ ${DISABLE_OFONO} == 1 ]
then
do_disable_ofono
else
    if [[ "${DISABLE_OFONO}" == "R" ]]
    then
    do_enable_ofono
    fi
fi

if [ ${DISABLE_AVAHI_DAEMON} == 1 ]
then
do_disable_avahi_daemon
else
    if [[ "${DISABLE_AVAHI_DAEMON}" == "R" ]]
    then
    do_enable_avahi_daemon
    fi
fi

if [ ${DISABLE_SYSTEMD_JOURNAL_FLUSH} == 1 ]
then
do_disable_systemd_journal_flush
else
    if [[ "${DISABLE_SYSTEMD_JOURNAL_FLUSH}" == "R" ]]
    then
    do_enable_systemd_journal_flush
    fi
fi

if [ ${DISABLE_SYSTEMD_RADNOM_SEED} == 1 ]
then
do_disable_systemd_random_seed
else
    if [[ "${DISABLE_SYSTEMD_RADNOM_SEED}" == "R" ]]
    then
    do_enable_systemd_random_seed
    fi
fi
if [ ${DISABLE_SYSTEMD_TIMESYNCD} == 1 ]
then
do_disable_systemd_timesyncd
else
    if [[ "${DISABLE_SYSTEMD_TIMESYNCD}" == "R" ]]
    then
    do_enable_systemd_timesyncd
    fi
fi
if [ ${DISABLE_SYSTEMD_NETWORKD} == 1 ]
then
do_disable_systemd_networkd
else
    if [[ "${DISABLE_SYSTEMD_NETWORKD}" == "R" ]]
    then
    do_enable_systemd_networkd
    fi
fi

if [ ${DISABLE_SYSTEMD_RFKILL} == 1 ]
then
do_disable_systemd_rfkill
else
    if [[ "${DISABLE_SYSTEMD_RFKILL}" == "R" ]]
    then
    do_enable_systemd_rfkill
    fi
fi
