#!/bin/bash

. ./send_stats.conf

if [ ${NETWORK_OPT_SWITCH} == 1 ]
then
systemctl unmask sytsemd-networkd
systemctl start sytsemd-networkd
sleep 4
fi
CICDID=$( head -n 1 cicdid.txt )
INSERT_DT=$( date +"%Y-%m-%dT%T" )

systemd-analyze | sed  's/(//g' | sed  's/)//g' | tr " " "\n"
systemd_analyze_output=( $( systemd-analyze | sed  's/(//g' | sed  's/)//g' | tr " " "\n" ) ) 

systemd_analyze_output="\"sa\" : { \"total:\":\"${systemd_analyze_output[9]}\", \" ${systemd_analyze_output[4]}\":\"${systemd_analyze_output[3]}\", \"${systemd_analyze_output[7]}\":\"${systemd_analyze_output[6]}\", \"${systemd_analyze_output[10]}\":\"${systemd_analyze_output[13]}\" }"

systemd_analyze__blame_output=$( systemd-analyze blame | sed 's/\\/\\\\/g' )
systemd_analyze__blame_output_data="\"sa_blame\": \""

time_part=1
tmp_for=""
for i in ${systemd_analyze__blame_output}
do
    if [ $time_part -eq 1 ]
    then
    tmp_for=$i
    time_part=0
    else
    time_part=1
    systemd_analyze__blame_output_data="${systemd_analyze__blame_output_data}${i};${tmp_for};"
    fi
done
systemd_analyze__blame_output_data="${systemd_analyze__blame_output_data::-1}\""

CURL_JSON="{\"cicdid\": \"${CICDID}\", \"insert_dt\":\"${INSERT_DT}\", \"edits\":\"${EDITS}\", ${systemd_analyze_output}, ${systemd_analyze__blame_output_data}}"
echo "${CURL_JSON}"
curl -XPOST -d "${CURL_JSON}" -H 'content-type: application/json' ${BACKEND_ADDRESS}/sendData
sleep 2
if [ ${NETWORK_OPT_SWITCH} == 1 ]
then
systemctl mask sytsemd-networkd
fi
if [ ${REBOOT_OPT_SWITCH} == 1 ]
then
sleep 2
reboot
fi