SUMMARY = "Appends script for sending startup stats."
LICENSE = "CLOSED"
SRC_URI += "file://cicdid.txt"
SRC_URI += "file://send_stats.sh"
SRC_URI += "file://send_stats.conf"
SRC_URI += "file://wired.network"

RDEPENDS_${PN} += "bash"


do_install(){
    echo ${CICDID}
    install -d ${D}/
    install -d ${D}/etc/systemd/network/
    install -m 0755 ${WORKDIR}/cicdid.txt ${D}/
    install -m 0755 ${WORKDIR}/send_stats.sh ${D}/
    install -m 0755 ${WORKDIR}/send_stats.conf ${D}/
    install -m 0644 ${WORKDIR}/wired.network ${D}/etc/systemd/network/
}

FILES_${PN} += "/cicdid.txt"
FILES_${PN} += "/send_stats.sh"
FILES_${PN} += "/send_stats.conf"
FILES_${PN} += "/etc/systemd/network/wired.network"