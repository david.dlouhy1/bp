SUMMARY = "Appends ASCII art of TUL logo."
LICENSE = "CLOSED"
SRC_URI += "file://tul.issue"

RDEPENDS_${PN} += "bash"

do_install(){
    install -d ${D}/
    install -d ${D}/etc
    install -m 0755 ${WORKDIR}/tul.issue ${D}/etc
}

FILES_${PN} += "/etc/tul.issue"