## Poseidon
Vyčleněný stroj pro Yocto a CI/CD.
Virtualizace: Hyper-V<br>
Lokace: Doma<br>
HYPER-V NAME: Poseidon<br>
CPU:1x<br>
RAM: 8192MB<br>
DISK: 127 virtual dynmic<br>
OS: Fedora server 35<br>
IP: 192.168.0.13<br>
HOSTNAME: poseidon<br>
PLUS SW: Guest agents<br>
LANGUAGE: Czech (Europe/Prague)<br>
USERS:<br>
	root -> Aa123456<br>
	david -> Aa123456 (člen skupiny wheel)<br>

Výpis lze získat skrze PS `Get-VM -Name Poseidon | Select-Object * | ConvertTo-Json` . Obdržíme data ve formátu JSON. Viz soubor `poseidon.json`

Ve stroji beží nainstalovaný Docker včetně GUI Portainer pro Docker.