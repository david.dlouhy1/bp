import './App.css';
import ShowCiCd from './routes/ShowCiCd';
import DatasetPicker from './routes/DatasetPicker';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import BuildList from './routes/BuildList';
function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/showCiCd" element={<ShowCiCd/>}/>
          <Route path="/buildList" element={<ShowCiCd/>}/>
          <Route path="/dataset" element={<DatasetPicker/>}/>
          <Route path="*" element={<BuildList/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
