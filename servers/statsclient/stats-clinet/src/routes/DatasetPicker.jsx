import React from 'react';
import {
    Link,
} from "react-router-dom";
import axios from 'axios';

import { MDBBtn, MDBCol, MDBContainer, MDBRow, } from 'mdb-react-ui-kit';

const download = (content, fileName, contentType) => {
    const a = document.createElement("a");
    const file = new Blob([content], { type: contentType });
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}
   
const onDownload = (type) => {
    switch(type){
        case "builds":
            axios.post("http://"+process.env.REACT_APP_BACKEND_ADDRESS+"/getAllCiCd", {}, {headers:{'Content-Type': 'application/json'}}).then(loadedData =>{
                download(JSON.stringify(loadedData.data), "dataset_builds.json", "text/plain");
            })
            break
        case "starts":
            axios.post("http://"+process.env.REACT_APP_BACKEND_ADDRESS+"/getAllStarts", {}, {headers:{'Content-Type': 'application/json'}}).then(loadedData =>{
                download(JSON.stringify(loadedData.data), "dataset_starts.json", "text/plain");
            })
            break
    }
   }

const GenerateNav = () => {
    return (
    <MDBContainer>
        <MDBRow>
        
            <MDBCol style={{textAlign:'left'}}>
                <MDBBtn className='mx-2' onClick={()=>window.location.reload()}>
                    <i className="fas fa-sync-alt"></i>
                </MDBBtn>
            </MDBCol>
            <MDBCol style={{textAlign:'right'}}>
                <Link to={"/ShowData"}>
                    {
                        <MDBBtn className='mx-2' color='danger'>
                            <i className="fas fa-times-circle"></i>
                        </MDBBtn>
                    }   
                </Link>
            </MDBCol>
        </MDBRow>
    </MDBContainer>
    )
}

const DatasetPicker = () =>{
    return(
    <>
    <br></br>
        {GenerateNav()}
        <MDBContainer style={{textAlign:'left'}}>
            <MDBRow>
                <MDBCol>
                    <h1 style={{textAlign:'center'}}>Dataset</h1><br/>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
        <MDBContainer style={{textAlign:'left'}}>
            <MDBRow>
                <MDBCol style={{textAlign:'center'}}>
                <br></br>
                    <br></br>
                    <h3 style={{textAlign:'center'}}>OS builds</h3><br/>
                    
                    <h1><i className="fas fa-download" onClick={()=>onDownload("builds")} style={{cursor:'pointer'}}></i></h1>
                </MDBCol>
                <MDBCol style={{textAlign:'center'}}>
                <br></br>
                    <br></br>
                    <h3 style={{textAlign:'center'}}>OS starts</h3><br/>

                    <h1><i className="fas fa-download" onClick={()=>onDownload("starts") } style={{cursor:'pointer'}}></i></h1>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    </>
    )
}
export default DatasetPicker;