import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
    Link,
  useLocation
} from "react-router-dom";
import Chart from 'react-apexcharts'
import ServiceTable from '../components/ServicesTable';
import { MDBBtn, MDBCol, MDBContainer, MDBIcon, MDBRow, MDBTable } from 'mdb-react-ui-kit';


const calulateStats = (dataset) => {
    var output = {
        'total_count':0,
        'average_boot_time':0,
        'average_kernel':0,
        'average_us':0,
        'fastest_id':0,
        'fastest_time':0,
        'fastest_dt':0,
        'fastest_text':0,
        'slowest_id':0,
        'slowest_time':0,
        'slowest_dt':0,
        'slowest_text':0
    }
    if(dataset&&dataset.startos){
        var start_sum=0;
        var kernel_sum=0;
        var ui_sum=0;
        var tmp_fastest_item={
            'id':0,
            'time':0,
            'dt':0,
            'text':""
        }
        var tmp_slowest_item={
            'id':0,
            'time':0,
            'dt':0,
            'text':""
        }

        output.total_count = dataset.startos.length
        dataset.startos.forEach(item => {
            if(tmp_fastest_item.id === 0)
            {
                tmp_fastest_item.id = item._id
                tmp_fastest_item.time = parseFloat(item.sa['total:'])
                tmp_fastest_item.dt = item['insert_dt']
                tmp_fastest_item.text = item['edits']
            }
            else{
                if(tmp_fastest_item.time > parseFloat(item.sa['total:'])){
                    tmp_fastest_item.id = item._id
                    tmp_fastest_item.time = parseFloat(item.sa['total:'])
                    tmp_fastest_item.dt = item['insert_dt']
                    tmp_fastest_item.text = item['edits']
                }
            }
            if(tmp_slowest_item.id === 0)
            {
                tmp_slowest_item.id = item._id
                tmp_slowest_item.time = parseFloat(item.sa['total:'])
                tmp_slowest_item.dt = item['insert_dt']
                tmp_slowest_item.text = item['edits']

            }
            else{
                if(tmp_slowest_item.time < parseFloat(item.sa['total:'])){
                    tmp_slowest_item.id = item._id
                    tmp_slowest_item.time = parseFloat(item.sa['total:'])
                    tmp_slowest_item.dt = item['insert_dt']
                    tmp_slowest_item.text = item['edits']
                }
            }
            start_sum+=parseFloat(item.sa['total:']);
            kernel_sum+=parseFloat(item.sa[' kernel']);
            ui_sum+=parseFloat(item.sa['userspace']);
        })
        
        output.average_boot_time = start_sum / output.total_count;
        output.average_kernel = kernel_sum / output.total_count;
        output.average_us = ui_sum / output.total_count;
        output.fastest_id = tmp_fastest_item.id;
        output.slowest_id = tmp_slowest_item.id;
        output.fastest_time = tmp_fastest_item.time;
        output.slowest_time = tmp_slowest_item.time;

        output.fastest_dt = tmp_fastest_item.dt;
        output.slowest_dt = tmp_slowest_item.dt;

        output.fastest_text = tmp_fastest_item.text;
        output.slowest_text = tmp_slowest_item.text;
    }
    return output
}

//Načte data a provede updateKomponenety skrze useEffect
const LoadData = (setData) =>{
    const filterJson = ParseFilterJson()
    useEffect(()=>{
        axios.post("http://"+process.env.REACT_APP_BACKEND_ADDRESS+"/getCiCdStarts", filterJson, {headers:{'Content-Type': 'application/json'}}).then(loadedData =>{
            setData(loadedData.data);
        })
    },[])
}

//Vyparsuje a vrátí ajo json ID z URL
const ParseFilterJson = () =>{
    let query = useQuery();
    const id =  query.get('id')
    return JSON.stringify({'id':id});
}

//Získá URL params
const useQuery =() => {
    const { search } = useLocation();
    return React.useMemo(() => new URLSearchParams(search), [search]);
}

//PŘevede string na milisekundy
const unitTimeStirngToMS = (stringRepresentation) =>{
    if(stringRepresentation.includes('us')){
        return parseFloat(stringRepresentation.replace("us-", ""))/1000;
    }
    else if(stringRepresentation.includes('ms')){
        return parseFloat(stringRepresentation.replace("ms-", ""));
    }
    else if(stringRepresentation.includes('s')){
        return parseFloat(stringRepresentation.replace("s-", ""))*1000;
    }
}

//připraví servicess array
const prepareServicesList= (data) =>{
    var result={}
    data.forEach(item =>{
        item.sa_blame.forEach(element=>{
             result[element.service] = []
         })
    })
    return result
}

//transofrmate servicess to graph
const transformServicesListForGraph = (data) => {
    var output = [];
    Object.keys(data).forEach(key => {
        output.push({name:key, data:data[key]})
      })
    output.forEach(item => {
        var trend =0
        var loadedData = item.data
        var lastTrend =loadedData[0]
        loadedData.forEach(yitem => {
            trend = trend + (lastTrend - yitem)
            lastTrend = yitem
        })
        trend=trend*(-1);
        item.trend=trend
    })
    return output;
}

//insert data to series
const fillServicesList= (data, list) =>{
    Object.keys(list).forEach(key => {
        //key
        //list[key]
        var used = false;
        data.sa_blame.forEach(element=>{
            if(key == element.service){
                list[key].push(element.time)
                used = true
            }
        })
        if(!used){
            list[key].push(0)
        }
      })
}

//Vyparsuje data pr graf
const prepareSeries = (loadedData) => {
    
   
    
    
    var categories = [];
    var services = [];

    var totalData = [];
    var totalTrend = 0;
    var lastForTotalTrend=0; 
    if(loadedData &&loadedData.startos){
        if((loadedData.startos[0])){
            lastForTotalTrend=unitTimeStirngToMS((loadedData.startos[0]).sa['total:'])
        }
    }

    var kernelData = [];
    var kernelTrend = 0;
    var lastForKernelTrend=0; 
    if(loadedData &&loadedData.startos){
        if((loadedData.startos[0])){
            lastForKernelTrend=unitTimeStirngToMS((loadedData.startos[0]).sa[' kernel'])
        }
    }

    var userspaceData = [];
    var userspaceTrend = 0;
    var lastForUserspaceTrend=0; 
    if(loadedData &&loadedData.startos){
        if((loadedData.startos[0])){
            lastForUserspaceTrend=unitTimeStirngToMS((loadedData.startos[0]).sa['userspace'])
        }
    }
    
    //vezm klíč
    //zjisti jestli je už v listu - pokud ne, tak vytvoř a obah je vždy prázdné pole

    //následně postupně projdi data a pro kaŽdý záznam z db projdi služby a doplň do listu hodnotu - pokud neosahuje, tak tento údaj uveď jaok 0

    if(loadedData && loadedData.startos)
    {
        services = prepareServicesList(loadedData.startos)
        loadedData.startos.forEach(item => {
            categories.push(item.insert_dt)

            let totalMs = unitTimeStirngToMS(item.sa['total:'])
            totalData.push(totalMs);
            totalTrend = totalTrend + (lastForTotalTrend - totalMs)
            lastForTotalTrend = totalMs

            let kernelMs = unitTimeStirngToMS(item.sa[' kernel'])
            kernelData.push(kernelMs);
            kernelTrend = kernelTrend + (lastForKernelTrend - kernelMs)
            lastForKernelTrend = kernelMs

            let userspaceMs = unitTimeStirngToMS(item.sa['userspace'])
            userspaceData.push(userspaceMs);
            userspaceTrend = userspaceTrend + (lastForUserspaceTrend - userspaceMs)
            lastForUserspaceTrend = userspaceMs

            fillServicesList(item, services);
        });
        totalTrend=totalTrend*(-1);
        kernelTrend=kernelTrend*(-1);
        userspaceTrend=userspaceTrend*(-1);
        services = transformServicesListForGraph(services)
    }

    var outputGraph1 = [
        {name:'total', data:totalData}
    ]

    var outputGraph2 = [
        {name:'kernel', data:kernelData},
        {name:'userspace', data:userspaceData}
    ]

    var output = [categories, outputGraph1, outputGraph2, services, totalTrend, kernelTrend, userspaceTrend]
    return output
}

//Komponenta - graf
const ShowCiCdGraph = ({loadedData}) =>{
    var catco=0;
    const series = loadedData;
    const categorySeries = series[0];
    const series1 = series[1];
    const series2 = series[2];
    const series3 = series[3];
    const options1 ={
        chart: {
            type: 'bar',
            stacked: true
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
        },
        plotOptions: {
            bar: {
              horizontal: true,
            },
        },
        xaxis: {
            categories: categorySeries,
            labels: {
                formatter: function (val) {
                  return val/1000 + "s"
                }
            }
        }
    };
    const options2 ={
        chart: {
            type: 'bar',
            stacked: true
        },
        legend: {
            position: 'top',
            horizontalAlign: 'left',
        },
        plotOptions: {
            bar: {
              horizontal: true,
            },
        },
        xaxis: {
            categories: categorySeries,
            labels: {
                formatter: function (val) {
                  return val/1000 + "s"
                }
            }
        }
    };
    return(
        <>
            <Chart options={options1} series={series1} type="bar"/>
            <Chart options={options2} series={series2} type="bar"/>
            <Chart options={options2} series={series3} type="bar"/>
        </>
    )
}
const GetElementForTrendValue = (value) =>{
    if(value>0){
        return <i class="fas fa-chevron-circle-up" style={{color:'red'}} ></i>
    }
    if(value<0){
        return <i class="fas fa-chevron-circle-down" style={{color:'green'}} ></i>
    }
    return <i class="fas fa-dot-circle" style={{color:'blue'}} ></i>
}

const GenerateDisableUI = (setData, setData2, data, data2) =>{
    let output = [];
    if(data && data.startos){
        data.startos.forEach(element => {
            output.push(
                <tr>
                    <td><small>{element._id}</small></td>
                    <td><small>{element.insert_dt}</small></td>
                    <td><small>{element.edits}</small></td>
                    <td style={{textAlign:'center'}}>
                    <small>
                        <MDBIcon 
                            icon="times-circle" 
                            style={{color:'red', cursor:'pointer'}} 
                            onClick={()=>{
                                for(var x = 0; x < data.startos.length; x++){
                                    if(data.startos[x]._id === element._id){
                                        data.startos.splice(x, 1);
                                        break;
                                    }
                                }
                                setData(data)
                                data2<1 ? setData2(1) : setData2(0)}
                            }
                        >
                        </MDBIcon></small>
                    </td>
                </tr>
            )
        })
    }
    output = 
    <MDBTable striped hover>
        <thead>
            <tr>
                <th>ID</th>
                <th>DateTime</th>
                <th>Label</th>
                <th style={{textAlign:'center'}}>Hide</th>
            </tr>
        </thead>
        <tbody>
            {output}
        </tbody>
    </MDBTable>;
    return output;
}

//Komponenta
const ShowCiCd = () => {
    const [data2, setData2] = useState(0);
    const [data, setData] = useState({
        cicd:{
            build_time: 0
        }
    });
    
    LoadData(setData)
    const series = prepareSeries(data);
    const stats = calulateStats(data)

    const totalTrend= series[4];
    const kernelTrend= series[5];
    const userspaceTrend= series[6];
    return( 
    <>
        <br></br>
        <MDBContainer>
        <MDBRow>
            <MDBCol style={{textAlign:'left'}}>
                <MDBBtn className='mx-2' onClick={()=>window.location.reload()}>
                    <i class="fas fa-sync-alt"></i>
                </MDBBtn>
            </MDBCol>
            <MDBCol style={{textAlign:'right'}}>
                <Link to={"/ShowData"}>{<MDBBtn className='mx-2' color='danger'>
                        <i class="fas fa-times-circle"></i>
                    </MDBBtn>}
                    
                </Link>
            </MDBCol>
        </MDBRow>
        </MDBContainer>
        <MDBContainer style={{textAlign:'left'}}>
            <MDBRow>
                <MDBCol>
                    <h1 style={{textAlign:'center'}}>CI/CD</h1><br/>
                    <MDBContainer>
                        <p>
                        ID: {data.cicd._id? data.cicd._id : "ID not selected"}<br/>
                        Build success: {data.cicd.build_success? "Yes" : "No or not selected"}<br/>
                        Build time: {data.cicd.build_time? data.cicd.build_time+" s" : "No or not selected"}<br/>
                        Pipeline ID: {data.cicd.ci_pipeline_iid? data.cicd.ci_pipeline_iid : "No or not selected"}<br/>
                        CICD pair ID: {data.cicd.cicd_pair_id? data.cicd.cicd_pair_id : "No or not selected"}<br/>
                        Insert datetime: {data.cicd.insert_dt? data.cicd.insert_dt : "No or not selected"}<br/>
                        Name of Yocto build: {data.cicd.rb_build? data.cicd.rb_build : "No or not selected"}<br/>
                        Name of Yocto image: {data.cicd.rb_image? data.cicd.rb_image : "No or not selected"}<br/><br/>
                        Deploy device: {data.cicd.deploy_device? data.cicd.deploy_device : "No or not selected"}<br/>
                        Deploy success: {data.cicd.deploy_success? "Yes" : "No or not selected"}<br/>
                        Deploy time: {data.cicd.deploy_time? data.cicd.deploy_time+" s" : "No or not selected"}<br/>
                        Deployed image: {data.cicd.deployed_image? data.cicd.deployed_image : "No or not selected"}<br/>
                        Deployment datetime: {data.cicd.insert_deploy_dt? data.cicd.insert_deploy_dt : "No or not selected"}<br/>
                        Image size: {data.cicd.size_of_deyploed_image? data.cicd.size_of_deyploed_image+" MB" : "No or not selected"}<br/>
                        </p>
                    </MDBContainer>
                </MDBCol>
                <MDBCol>
                    <h1 style={{textAlign:'center'}}>OS starts</h1><br/>
                    <MDBContainer>
                        <p>
                        Total count: {stats.total_count}<br/>
                        Average boot time: {stats.average_boot_time}<br/>
                        Average kernel: {stats.average_kernel}<br/>
                        Average us: {stats.average_us}<br/><br/>
                        Fastest start
                        <ul>
                            <li>ID: {stats.fastest_id}</li>
                            <li>Time: {stats.fastest_time}s</li>
                            <li>DateTime: {stats.fastest_dt}</li>
                            <li>Text: {stats.fastest_text}<br/></li>
                        </ul>
                        Slowest start
                        <ul>
                            <li>ID: {stats.slowest_id}</li>
                            <li>Time: {stats.slowest_time}s</li>
                            <li>DateTime: {stats.slowest_dt}</li>
                            <li>Text: {stats.slowest_text}<br/></li>
                        </ul>
                        </p>
                    </MDBContainer>
                </MDBCol>
            </MDBRow>
            <hr></hr>
            <MDBRow>
                <MDBCol>
                    <MDBContainer>
                        <b>Selected items:</b><br/>
                        {GenerateDisableUI(setData, setData2, data, data2)}
                    </MDBContainer>
                </MDBCol>
            </MDBRow>
            <hr></hr>
            <MDBRow>
                <MDBCol>
                    <MDBContainer>
                        <b>Trends:</b><br/>
                        <MDBRow>
                            <MDBCol style={{textAlign:'center'}}>Total: {GetElementForTrendValue(totalTrend)} <small>({totalTrend})</small><br/></MDBCol>
                            <MDBCol>Kernel: {GetElementForTrendValue(kernelTrend)} <small>({kernelTrend})</small><br/></MDBCol>
                            <MDBCol>Userspace: {GetElementForTrendValue(userspaceTrend)} <small>({userspaceTrend})</small><br/></MDBCol>
                        </MDBRow>               
                    </MDBContainer>
                </MDBCol>
            </MDBRow>
            <ShowCiCdGraph loadedData={series}></ShowCiCdGraph>
            <ServiceTable data={series}></ServiceTable>
        </MDBContainer>
    </>)
}

export default ShowCiCd;