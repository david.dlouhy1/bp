import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { MDBBtn, MDBCol, MDBContainer, MDBIcon, MDBRow, MDBTable } from 'mdb-react-ui-kit';
import { Link } from 'react-router-dom';

const getTableRows = (data) =>{
    var output = [];
    var build_count=0;
    var starts_count=0;
    if(data){
    data.forEach(element=>{
        var icon = element.success_build ? <MDBIcon icon="check-circle" style={{color:'green'}}/> : <MDBIcon icon="times-circle" style={{color:'red'}} />;
        starts_count+=element.startos_count;
        output.push(
        <tr key={build_count}>
            <td><Link to={"/showCiCd?id="+element.id}>{element.id}</Link></td>
            <td>{element.dt}</td>
            <td>{icon}</td>
            <td>{element.startos_count}</td>
        </tr>
        )
        build_count++;
    })}
    return [output, build_count, starts_count];
}

//GetIDs with datetime from build db
const GetDataForList = (setData, setZeroData, emptyStartFilter) =>{
    useEffect(()=>{
        axios.post("http://"+process.env.REACT_APP_BACKEND_ADDRESS+"/getAllCiCd", {}, {headers:{'Content-Type': 'application/json'}}).then(loadedData =>{
            let data = []
            for( var i = 0; i < loadedData.data.length; i++){ 
                if ( (loadedData.data[i]).startos_count != 0 && emptyStartFilter === true) { 
                    data.push(loadedData.data[i]);
                }
            
            }
            setData(loadedData.data);
            setZeroData(data);
        })
    },[])
};




//Komponenta pro pŘehled všech buildů
const BuildList = () => {
    const [emptyStartFilter, setEmptyStartFilter] = useState(true);
    const [data, setData] = useState();
    const [zeroData, setZeroData] = useState();
    const [outData=zeroData, setOutData] = useState();
    GetDataForList(setData, setZeroData, emptyStartFilter)
    var [rows, build_count, starts_count] = getTableRows(outData);
    return(
    <div>
        <h1 style={{textAlign:'center'}}>Build List</h1><br/>
        Build count: {build_count}<br/>
        Starts count: {starts_count}<br/>
        Average stats for build: {starts_count/build_count}<br/>
        <br/>
        <MDBContainer>
            <MDBRow>
                <MDBCol><Link to={"/dataset"}><div className="d-grid gap-2"><MDBBtn>Dataset</MDBBtn></div></Link></MDBCol>
                <MDBCol><div className="d-grid gap-2"><MDBBtn onClick={()=>{
            setEmptyStartFilter(!emptyStartFilter);
            emptyStartFilter ?setOutData(data) : setOutData(zeroData) ;
            }}>{emptyStartFilter ? "Enable" : "Disable" } empty starts</MDBBtn></div></MDBCol>
            </MDBRow>
        </MDBContainer>
        <MDBTable striped hover id={'buildTable'}>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>DateTime</th>
                    <th>Success</th>
                    <th>Starts</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </MDBTable>
    </div>
    )
};

export default BuildList;