import { MDBTable } from 'mdb-react-ui-kit';
const average = list => list.reduce((prev, curr) => prev + curr) / list.length;

const GetElementForTrendValue = (value) =>{
    if(value>0){
        return <i class="fas fa-chevron-circle-up" style={{color:'red'}} ></i>
    }
    if(value<0){
        return <i class="fas fa-chevron-circle-down" style={{color:'green'}} ></i>
    }
    return <i class="fas fa-dot-circle" style={{color:'blue'}} ></i>
}

const ServiceTableRow = (dataForRow) => {
    const name = dataForRow.data.name
    const min = dataForRow.data.min
    const max = dataForRow.data.max
    const average = dataForRow.data.average
    const trend = dataForRow.data.trend
    return(
    <tr>
        <td>{name}</td>
        <td>{min}</td>
        <td>{max}</td>
        <td>{average}</td>
        <td>
            {GetElementForTrendValue(trend)}
            <small> ({trend})</small>
        </td>
        <td style={{display:"none"}}>
            {trend}
        </td>
    </tr>);
}

//Vrátí statistky služby
const getStats =(dataVector) =>{
    var output = [];
    dataVector.forEach(element => {
        output.push({
            'name':element.name,
            'min':Math.min(...element.data),
            'max':Math.max(...element.data),
            'average':average(element.data),
            'trend':element.trend
        });
    });
    return output
}

//Vrátí řádky tabulky
const getTableRows = (data) =>{
    var output = [];
    if(data){
    data.forEach(element=>{
        output.push(<ServiceTableRow data={element}></ServiceTableRow>);
    })}
    return output;
}

function sortTable(col, descTrue) {
    var table, rows, switching, i, x, y,  shouldSwitch;
    table = document.getElementById("myTable");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
      // Start by saying: no switching is done:
      switching = false;
      rows = table.rows;
      /* Loop through all table rows (except the
      first, which contains table headers): */
      for (i = 1; i < (rows.length - 1); i++) {
        // Start by saying there should be no switching:
        shouldSwitch = false;
        /* Get the two elements you want to compare,
        one from current row and one from the next: */
        x = rows[i].getElementsByTagName("td")[col].innerHTML;
        y = rows[i + 1].getElementsByTagName("td")[col].innerHTML;
        var flx = (parseFloat(x.toLowerCase()))
        var fly =(parseFloat(y.toLowerCase()))
        if(isNaN(flx) || isNaN(fly))
        {
            // Check if the two rows should switch place:
            if (((!descTrue) && (x.toLowerCase() < y.toLowerCase())) ||  (descTrue && (x.toLowerCase() > y.toLowerCase()))) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        else{
            
            // Check if the two rows should switch place:
            if (((!descTrue) && (flx < fly)) ||  (descTrue && (flx > fly))) {
                
            // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        
        
      }
      if (shouldSwitch) {
        /* If a switch has been marked, make the switch
        and mark that a switch has been done: */
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
      }
    }
  }



const ServiceTable = ({data}) =>{
    var descTrue = false;
    const rows = getTableRows(getStats(data[3]))
    return(
        <MDBTable striped hover id={'myTable'}>
            <thead>
                <tr>
                    <th style={{cursor:'pointer'}} onClick={()=>{descTrue=!descTrue; sortTable(0, descTrue)}}>Service</th>
                    <th style={{cursor:'pointer'}} onClick={()=>{descTrue=!descTrue; sortTable(1, descTrue)}}>T-Min</th>
                    <th style={{cursor:'pointer'}} onClick={()=>{descTrue=!descTrue; sortTable(2, descTrue)}}>T-Max</th>
                    <th style={{cursor:'pointer'}} onClick={()=>{descTrue=!descTrue; sortTable(3, descTrue)}}>T-Avg</th>
                    <th style={{cursor:'pointer'}} onClick={()=>{descTrue=!descTrue; sortTable(5, descTrue)}}>Trend</th>
                    <th style={{display:"none"}} >T-index</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </MDBTable>
    )
}
export default ServiceTable;