import React from "react";
import { MDBTable } from 'mdb-react-ui-kit';

function Table({ table }) {
    const { headers, rows } = table;
  
    const [selectedHeaderIndex, setSelectedHeaderIndex] = React.useState(0);
    const [sortDirection, setSortDirection] = React.useState("asc");
  
    const comparator =
      sortDirection === "asc"
        ? (row1, row2) =>
            row1[selectedHeaderIndex].localeCompare(row2[selectedHeaderIndex])
        : (row1, row2) =>
            row2[selectedHeaderIndex].localeCompare(row1[selectedHeaderIndex]);
  
    const flipSortDirection = () => (sortDirection === "asc" ? "desc" : "asc");
  
    const sortedRows = rows.sort(comparator);
  
    return (
    <MDBTable striped hover>
        <thead>
          {headers.map((header, i) => (
            <th
              onClick={() => {
                setSelectedHeaderIndex(i);
                setSortDirection(
                  selectedHeaderIndex === i ? flipSortDirection() : "asc"
                );
              }}
            >
              {header}
            </th>
          ))}
        </thead>
        <tbody>
          {sortedRows.map(row => (
            <tr>
              {row.map(cell => (
                <td>{cell}</td>
              ))}
            </tr>
          ))}
        </tbody>
    </MDBTable >
    );
  }
  

  
  export default Table