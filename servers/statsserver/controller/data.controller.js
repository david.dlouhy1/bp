const { ObjectId } = require("mongodb");

//PŘevede string na milisekundy
const unitTimeStirngToMS = (stringRepresentation) =>{
    if(stringRepresentation.includes('us')){
        return parseFloat(stringRepresentation.replace("us-", ""))/1000;
    }
    else if(stringRepresentation.includes('ms')){
        return parseFloat(stringRepresentation.replace("ms-", ""));
    }
    else if(stringRepresentation.includes('s')){
        return parseFloat(stringRepresentation.replace("s-", ""))*1000;
    }
}

//Vloží data do databáze
exports.sendData = async (req, res, client) => {
    res.header("Access-Control-Allow-Origin", "*");
    try {
        const connection = await client.connect();
        const db = await connection.db("bp");
        const collection = await db.collection("startos");

        var jsonData = req.body;
        jsonData.insert_dt = new Date(jsonData.insert_dt)

        var sa_blame = []
        var service = true;
        var tmp_item={};
        jsonData.sa_blame.split(';').forEach(item => {
            if(service){
                tmp_item.service=item;
                service=false;
            }
            else{
                tmp_item.time=unitTimeStirngToMS(item);
                sa_blame.push(tmp_item)
                service = true;
                tmp_item={}
            }
        });
        jsonData.sa_blame=sa_blame;
        await collection.insertOne(jsonData)
    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
    res.status(200).send({message:"Data inserted."});
}

// Získá data pro analýzu - konkrétně jendotlivé CICD s všech grafy startů
exports.getCiCdStarts = async (req, res, client) =>{
    try{
        const connection = await client.connect();
        const db = await connection.db("bp");
        const collection_cicd = await db.collection("cicd");
        const collection_startos = await db.collection("startos");
        
        var query_sa = {'_id': new ObjectId(req.body.id)};
        var query_sa_blame = {'cicdid': req.body.id};

        var result_sa = await collection_cicd.findOne(query_sa)
        var result_sa_blmabe = await collection_startos.find(query_sa_blame).toArray()

        res.status(200).send({'cicd':result_sa, 'startos':result_sa_blmabe})
    }
    catch(err){
        console.log(err);
        res.status(400).send({'message': 'Error!'});
    }
    finally {
        await client.close();
    }
}

// Odešle jako repsonse datum, id, success build, poČet všech startů CICD buildu pro klienta
exports.getAllCiCd = async (req, res, client) => {
    try{
        var data = [];
        const connection = await client.connect();
        const db = await connection.db("bp");
        const collection_cicd = await db.collection("cicd");
        const collection_startos = await db.collection("startos");

        var query_cicd = {projection: {'_id':1, 'build_success':1, 'insert_dt':1}, sort:{'insert_dt':-1}};

        var result_cicd = await collection_cicd.find({},query_cicd).toArray();
        for (const element of result_cicd) {
            var query_startos= {'cicdid': element._id.toString()};
            var result_startos = await collection_startos.countDocuments(query_startos)
            data.push({
                'id':element._id,
                'dt':element.insert_dt,
                'success_build':element.build_success,
                'startos_count':result_startos
            })
          }
        res.status(200).send(data)
    }
    catch(err){
        console.log(err);
        res.status(400).send({'message': 'Error!'});
    }
    finally{
        await client.close();
    }
}

// Odešle jako repsonse datum, id, success build, poČet všech startů CICD buildu pro klienta
exports.getAllStarts = async (req, res, client) => {
    try{
        var data = [];
        const connection = await client.connect();
        const db = await connection.db("bp");
        const collection_startos = await db.collection("startos");
        var result_startos = await collection_startos.find({}).toArray();
        res.status(200).send(result_startos)
    }
    catch(err){
        console.log(err);
        res.status(400).send({'message': 'Error!'});
    }
    finally{
        await client.close();
    }
}