const express = require('express')
const app = express()
const cors = require('cors');
const port = 3000
require('dotenv').config();
const env = process.env;
const { MongoClient } = require('mongodb');
const uri = env.mongo_url;
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(express.json());

app.use(cors({
  origin: [
    env.CORS_ENABLE_ENDPOINT
  ]
}));

app.get('/', (req, res) => {
  res.send('Welcome to state-server API')
})

app.listen(port, () => {
  console.log(`App listening on port ${port}`)
  if(env.PERMIT_PUBLIC_WRITE != "n"){
    require('./routes/sendData.route')(app, client);
  }
  require('./routes/getCiCdStarts.route')(app, client);
  require('./routes/getAllCiCd.route')(app, client);
  require('./routes/getAllStarts.route')(app, client);
})