.. bp documentation master file, created by
   sphinx-quickstart on Thu Mar 24 19:00:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BP Optimalizace startu OS Linux - Dokumentace
=============================================
Tato dokumentace se vztahuje k bakalářské práci Davida Dlouhého, studenta FM TUL. Obsahem je dokumentace optimalizací, popis konfigurací a postup sestavení celého ekosystému.

.. toctree::
   :maxdepth: 2
   :caption: Abstrakt:
   
   pages/abstract/abstract

.. toctree::
   :maxdepth: 2
   :caption: Úvodní upozornění:

   pages/basic_warn/basic_warn

.. toctree::
   :maxdepth: 2
   :caption: Předpoklady:

   pages/prerequisite/prerequisite

.. toctree::
   :maxdepth: 2
   :caption: Správa kódu:

   pages/version_tool/version_tool

.. toctree::
   :maxdepth: 2
   :caption: Ekosystém:

   pages/ecosystem/ecosystem
   pages/ecosystem/ecosystem_create
   pages/ecosystem/ecosystem_usecase
   pages/ecosystem/ecosystem_common_problems

.. toctree::
   :maxdepth: 2
   :caption: Automatizace:

   pages/automation/automation

.. toctree::
   :maxdepth: 2
   :caption: Optimalizace:

   pages/optimization/optimization

.. toctree::
   :maxdepth: 2
   :caption: Dokumentace:

   pages/docs/docs

.. toctree::
   :maxdepth: 2
   :caption: Kontakt:

   pages/contact/contact