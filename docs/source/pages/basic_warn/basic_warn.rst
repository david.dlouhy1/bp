Bezpečnostní upozornění
=======================
Při práci s vývojovou deskou dbejte na základní bezpečnost práce se zařízeními využívajícími elektrickou energii. V opačném případě může dojít ke zranění či k poškození zařízení. Taktéž je třeba bezpečně manipulovat s SD kartou.

Při používání privátních klíčů a jiných bezpečnostních prvků je třeba dbát na.

Špatná konfigurace skrze projekt Yocto může vést k poškození zařízení. Taktéž je třeba důsledně dodržet konfiguraci a implementaci automatizačního systému a vývojového ekosystému.

Licenční ustanovení
===================
Veškeré licenční ustanovení vycházejí z nařízení `Fakulty mechatroniky, informatiky a mezioborových studií Technické univerzity v Liberci <https://www.fm.tul.cz/>`_. 