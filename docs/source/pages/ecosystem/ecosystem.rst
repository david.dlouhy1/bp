Popis ekosystému
================

Pro vývoj s podporou automatizace a datové analýzy byl vytvořen komplexní ekosystém. Tento ekosystém poskytuje při správném používání cestu jak ušetřit čas a pracovat efektivně.

Uložiště dat
------------

Data z jednotlivých měření startů OS a z běhů CI/CD je třeba uchovat pro následné zpracování a analýzu. Tato data jsou uchovávána v NoSQL databázi `MongoDB <https://www.mongodb.com/>`_. Tato dokumentová databáze poskytuje značnou míru flexibility pro budoucí úpravy počtu analyzovaných položek.

Vývojové prostředí
------------------

Oddělené vývojové prostředí uvnitř kontejnerizační služby `Docker <https://www.docker.com/>`_ s integrovaným projektem Yocto pomůže se systematickým vývojem Linuxové distribuce v čistém prostředí. Zároveň jde toto oddělené prostředí použít při automatizaci.

Prostředí datové analýzy
------------------------

Data jsou z datového uložiště poskytnuta backend a fronted NodeJS aplikaci. Tyto aplikace se starají o práci uložištěm, s vizualizací dat a s datovou analýzou.