Sestavení ekosystému
====================

Tato sekce popisuje postup, jak si sestavit kompletní ekosystém určený k vývoji. Návod se opírá o předem uvedené informace, proto je doporučeno si přečíst předešlé sekce dokumentace.

Příprava uložiště dat
---------------------

Existuje více možností, jak si uvést do provozu databázi, která se stará o správné uchování dat. Mezi volby se řadí: čistá instalace do systému, nasazení DB jako kontejner nebo možnost verze MongoDB pro cloud, která se jmenuje `MongoDB Atlas <https://www.mongodb.com/cloud>`_. Tento návod popisuje, jak si připravit uložiště dat s využitím `MongoDB Atlas <https://www.mongodb.com/cloud>`_.

Nejprve je třeba si založit účet na `oficiálních stránkách <https://www.mongodb.com/cloud/atlas/register1>`_. Po vytvoření účtu následuje příprava projektu, tento projekt by se měl jmenovat **Artemis**. Uvnitř projektu si vytvoříme databázi s názvem **bp**. S touto databází budeme nadále pracovat a bude obsahovat dvě další kolekce (cicd, startos). Dále je třeba vytvořit uživatele **gitlabcicd** s metodou autentifikace SCRAM a s právy pro čtení a zápis minimálně na všechny databáze uvnitř projektu Artemis. Uživatelské ověřovací údaje si poznamenejte a udržte v bezpečí.

Příprava vývojového prostředí
-----------------------------

Implementace vývojového prostředí je navržená a popsaná pro hostovací operační systém Windows 10 Pro s funkční službou `Hyper-V <https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/about/>`_. Nicméně za použití alternativních služeb jde provozovat prostředí multiplatformně.

Nejprve je třeba vytvořit virtuální stroj s názvem **Poseidon** tento stroj doporučené parametry stroje naleznete v `repositáři <https://gitlab.com/david.dlouhy1/bp/-/tree/main/VM>`_. Po zprovoznění virtuálního stroje si doinstalujte službu `Docker <https://docs.docker.com/engine/install/fedora/>`_ a nástroj `docker-compose <https://docs.docker.com/engine/install/fedora/>`_ dle oficiální dokumentace. Důležité je také, aby bylo umožněno pracovat se službou Docker alespoň jedním non-root uživatelem. Další akce proto provádějte na tomto non-root uživateli.

V případě, že máte připravené prostředí, můžete naklonovat repositář. Projekt je navržen pro cestu ``/home/david/git``. ::

    git clone https://gitlab.com/david.dlouhy1/bp.git

Po naklonování přejděte do tools, kde naleznete pomocné skripty. Spusťte skript na sestavení images pro Docker. ::
    
    cd tools
    sh build_docker_images.sh
    cd ..

Dále je potřeba naklonovat pracovní vrstvy pomocí připraveného skriptu. ::

    sh clone-yocto.sh

Pokud vše funguje jak má, tak se můžete přepnout do vývojového prostředí pomocí skriptu start.sh v kořenovém adresáři. ::

    sh start.sh -n

Příprava analyzačního prostředí
-------------------------------

Pro spuštění plnohodnotného produkčního prostředí je třeba udělat následující kroky.

Nejprve jděte v naklonovaném repozitáři do ``docker/statsserver-machine``. Zde vytvořte soubor s názvem ``statsserver.env``. ::

    cd docker/statsserver-machine
    vim statsserver.env

Soubor musí obsahovat následující položky:

- ``REPO_LOCATION``
    - Lokace naklonovaného repozitáře v souborovém systému
    - Např. ``"/home/david/git"`` 
- ``PERMIT_PUBLIC_WRITE``
    - Povolení možnosti vkládání dat
    - Zadejte ``"y"`` pokud chcete zasílat data
    - Zadejte ``"n"`` pokud nechcete zasílat data
    - V případě jiné hodnoty systém automaticky povolí vkládání dat!
- ``MONGO_URL``
    - Zde bude vaše url pro připojení k MongoDB v cloudu
    - Např. ``"mongodb+srv://username:password@server/db?retryWrites=true&w=majority"``
- ``PRODUCTION``
    - Tato proměnná pomůže s nasazením produkční verze
    - Pokud aplikaci chcete spustit a neměnit jí = chcete produkční verzi, pak zadejte  ``"y"`` a aplikace se automaticky sestaví a spustí.
    - Pokud chcete pracovat na vývoji, produkční verzi použít nesmíte. Proto zadejte ``"n"``, ale počítejte s tím, že aplikaci musíte spustit manuálně a taktéž spustit skript ``install_modules.sh`` pro backend i frontend. Skript naleznete vždy v adresáři aplikace v ``tools``.
    - V případě jiné hodnoty dojde k nasazení vývojové verze.
- ``CORS_ENABLE_ENDPOINT``
    - Informace o tom, z jaké adresy bude frontend přistupovat na backend.
    - Např. ``"http://192.168.0.13:3224"``

Soubor uložte. Dále jděte do ``servers/statsclient/stats-clinet`` a zde vytvořte soubor ``.env``. ::

    cd ../../servers/statsclient/stats-clinet
    vim ./.env

Obsah souboru musí být následující:

- ``REACT_APP_BACKEND_ADDRESS``
    - Adresa backend aplikace
    - Např. ``192.168.0.13:3223``

Soubor uložte a přesuňte se do adresáře ``tools`` v kořenovém adresáři repozitáře. Zde spusťte skript s názvem ``start_statsservermachine.sh``. Skript spustí s pomocí nástroje ``docker-compose`` stack aplikace. ::

    cd ../../../tools
    sh start_statsserver.sh

Takto je hotovo a aplikace by měla v případě produkční verze běžet a být volně přístupná:

- Backend
    - Port **3223**
    - Např. `192.168.0.13:3223 <http://192.168.0.13:3223>`_
- Frontend
    - Port **3224**
    - Např. `192.168.0.13:3224 <http://192.168.0.13:3224>`_


U produkční verze se připojte interaktivně do kontejneru a spusťte manuálně ::

    docker exec -i statsserver-machine_stats-server_1 bash -c "cd /statsserver/tools; sh install_moduels.sh; cd ..; node server"
    docker exec -i statsserver-machine_stats-client_1 bash -c "cd /statsclinet/stats-clinet/tools; sh install_moduels.sh; cd ..; npm start"

