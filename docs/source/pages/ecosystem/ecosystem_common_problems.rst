Časté problémy ekosystému
=========================

Bude doplněno po zpětné vazbě.

Chyby uložiště dat
------------------

Bude doplněno po zpětné vazbě.

Chyby vývojového prostředí
--------------------------

Bude doplněno po zpětné vazbě.

Chyby analyzačního prostředí
----------------------------

Bude doplněno po zpětné vazbě.

**Spustil jsem sestavení, ale aplikace neběží.**

Počkejte minimálně 5 minut. Někdy se stává, že sestavení chvíli trvá.
