Struktura GitLab repositáře
===========================
Repositář obsahuje uspořádané soubory do adresářů dle zaměření. Mezi podstatné patří níže vybrané. Repositář je k dispozici na adrese `https://gitlab.com/david.dlouhy1/bp <https://gitlab.com/david.dlouhy1/bp>`_.

**Kořenový adresář**

* Základní obslužné skripty
* Soubory pro git a GitLab

**docker**

* Soubory Dockerfile pro automatizaci a vývoj
* Soubory docker-compose pro automatizaci a vývoj 

**docs**

* Zdroje pro dokumentaci
* Nástroje pro generování různých typů dokumentace

**servers**

* Backend NodeJS aplikace pro datovou analýzu
* Frontend NodeJS aplikace pro datovou analýzu

**tools**

* Užitečné nástroje
* Nástroje pro vývoj v Yocto
* Obsluha sestavení
* Obsluha nasazení

**VM**

* Informace o Hyper-V stroji

**workspace**

* Pracovní adresář pro Yocto
* Zdrojové vrstvy
* Jednotlivá sestavení
* Recepty pro Yocto

Význam verzovacích větví
========================
Kód je organizován do jednotlivých větvích. Tyto větve mají počáteční obor názvů, který pomůže s automatizací.

Přehled pravidel:

    ``/feature/..``
        Přidání nové funkcionality a práce na optimalizacích
    ``/docs/..``
        Práce na dokumentaci

Pokud bychom tedy pracovali na novém sestavení OS a zároveň bychom pracovali dokumentaci, mohla by struktura větví vypadat následovně: ::

    main
    |
    |--> feature/optim-2
              ||
              ||--> docs/optim-2
              |---> feature/optim-2-yocto-build-work/rpi-fast-boot

Popis by pak byl následující. Vycházíme z hlavní větve ``main``. Z této větvě vychází ``feature/optim-2``. V této větvi budeme dělat optimalizaci č. 2. Práce na konkrétním obrazu OS (image) bude následně ve větvi ``feature/optim-2-yocto-build-work/rpi-fast-boot``, která vychází z ``feature/optim-2``. Dokumentace k této optimalizaci bude ve větvi ``docs/optim-2``, která vychází z ``feature/optim-2``.

Jak pracovat s GitLab repositářem?
==================================
Jako běžný návštěvník máte možnost pouze repositář klonovat a prohlížet si jej. Naleznete jej na adrese `http://docs.dlouhybp.tkkzmj.cz/ <http://docs.dlouhybp.tkkzmj.cz/>`_. Taktéž si můžete přepínat mezi jednotlivými vývojovými větvemi. Součástí je taktéž `GitLab Wiki <https://gitlab.com/david.dlouhy1/bp/-/wikis/home>`_, která je alternativou k externí dokumentaci.