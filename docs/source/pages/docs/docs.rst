Struktura dokumentace
=====================

Existují dvě verze dokumentace. `Externí webová dokumentace <http://docs.dlouhybp.tkkzmj.cz/>`_ a `GitLab Wiki <https://gitlab.com/david.dlouhy1/bp/-/wikis/home>`_. Dokumentace je primárně členěna dle jednotlivých témat, která jsou řazena dle toho, jak by je uživatel měl číst. 

Práce s dokumentací
===================

Je doporučeno pracovat s dokumentací chronologicky a vždy ověřit, že daný postup nefunguje. U externích technologií je třeba pohlídat konzistenci nových verzí. Při nalezení problému se obraťte na autora práce - viz :doc:`sekce Kontakt <../contact/contact>`.

Automatizace generování a publikace dokumentace
===============================================

Pokud máte nastavení automatizační prostředí, pak stačí pouze provést push změn do repositáře a dokumentace se automaticky sestaví a nasadí. Detaily jsou vidět v souboru `.gitlab-ci.yml <https://gitlab.com/david.dlouhy1/bp/-/blob/main/.gitlab-ci.yml>`_.