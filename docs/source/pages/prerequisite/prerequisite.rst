Předpokládané vědomosti
=======================
Předpokládá se středoškolské vzdělání - optimálně odborné se zaměřením na IT. Předpokládá se, že čtenář zná a ovládá Yocto Project, Docker, Hyper-V, správu Windows a Linux, MongoDB, základy sítí a základy verzovacího systému git a GitLab.

Předpokládané HW vybavení
=========================
Vyžaduje se minimálně pracovní stanice (PC) s průměrným CPU, s alespoň 16GB RAM DDR3 a dostatečně velkým diskem, kde je minimálně 200GB volného místa. Ideálně by měl být disk SSD, proces kompilace je pak rychlejší.

Tato práce využívá externí RaspberryPi 4, které se stará o nasazení připraveného obrazu OS na SD kartu.

Předpokládané SW vybavení
=========================
K vývoji na pracovní stanici stačí editor Visual Studio Code a virtualizační platforma Hyper-V. Z toho také plyne požadavek na Windows Pro OS (ideálně 10). Na Raspberry Pi 4 je doporučenou použít OS Fedora Workstation 35 s podporou pro Python.

Předpokládán je také přístup k internetu.