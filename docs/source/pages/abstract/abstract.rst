Obsah dokumentace
=================
Dokumentace popisuje, jak optimalizovat start operačního systému Linux. Uvedeny jsou konkrétní vybrané optimalizace. Obecné informace jsou k dispozici v textové podobě bakalářské práce. Je zde také popsáno jak si sestavit a jako pracovat s vývojovém prostředí založeném na `projektu Yocto <https://www.yoctoproject.org/>`_.

Tato dokumentace obsahuje :doc:`úvodní sekci <../abstract/abstract>`, kde se dozvíte jak tuto dokumentaci efektivně využít.

Dále se pak dozvíte :doc:`základní bezpečností upozornění <../basic_warn/basic_warn>`, kterým je dobré věnovat pozornost již na začátku. Nedílnou součástí jsou i :doc:`licenční ustanovení <../basic_warn/basic_warn>`.

:doc:`Předpoklady <../prerequisite/prerequisite>` řeknou čtenáři co je dobré vědět, umět a mít k dispozici před tím, než se pustí do práce.

Během jednotlivých optimalizací došlo k vytvoření spousty podpůrných skriptů, konfiguračních souborů a mnoha další nástrojů. Vše se nachází v centralizovaném repositáři `centralizovaném repositáři <https://gitlab.com/david.dlouhy1/bp>`_. Tato dokumentace se také částečně věnuje struktuře toho repositáře a to v sekci :doc:`Správa kódu <../version_tool/version_tool>`. Všechny tyto nástroje pak tvoří :doc:`ekosystém <../ecosystem/ecosystem>`, který se dá využít na optimalizaci a na následnou analýzu startů operačního systému Linux.

Ekosystém lze také napojit na efektivní automatizaci. Po přečtení :doc:`této sekce <../automation/automation>` bude čtenář seznámen s napojením existujícího ekosystému na již existující GitLab repositář a optimalizační ekosystém.

Sekce :doc:`Optimalizace <../optimization/optimization>` zaštiťuje vybrané optimalizace. Popisuje jejich praktickou aplikaci a jejich význam.

Závěrem jsou uvedeny :doc:`kontaktní údaje <../contact/contact>` a informace, které se týkají k :doc:`psaní této dokumentace <../docs/docs>` a k její následné publikaci.

Jak pracovat s dokumentací?
===========================
Témata jsou na sebe návazná a proto stačí, když si projdete postupně celou dokumentaci, Pokud nepoužíváte verzi dokumentace Wiki, pak můžete využít možnost vyhledávání. Součástí je také navigace, kde jsou jednotlivá témat shlukována do sekcí. Tuto navigaci můžete také využít pro snadné nalezení všeho, co potřebujete.