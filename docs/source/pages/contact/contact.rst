Autor
=====

Jméno: David Dlouhý

* e-mail: `david.dlouhy@tul.cz <mailto:david.dlouhy@tul.cz>`_

Vedoucí práce
=============

Jméno: Ing. Lenka Kosková Třísková, Ph.D.


* kontaktní údaje: `Web FM TUL <https://www.fm.tul.cz/personal/lenka.koskova.triskova>`_