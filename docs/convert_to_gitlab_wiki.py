import os

delimeter = '/'
workdir = '.'+delimeter+'build'+delimeter+'markdown'
f_ext = ".md"
index_file = workdir+delimeter+'index'+f_ext
sidebar_file = workdir+delimeter+'_sidebar'+f_ext

starts_title="# "
starts_section="* "
starts_subsection="    * "
title_key='header'
section_key='section'
subsection_key='subsection'
def remove_ext_md():
    for foldersfiles in os.walk(workdir):
        for folderfiles in foldersfiles[2]:
            actual_file = foldersfiles[0]+delimeter+folderfiles
            if(os.path.splitext(actual_file)[-1].lower()==f_ext):
                lines=[]
                with open(actual_file, 'r+', encoding='utf-8') as actual_file_handler:
                    lines = actual_file_handler.readlines()
                with open(actual_file, 'w', encoding='utf-8') as actual_file_handler:
                    for line in lines:
                        line = line.replace(".md","")
                        actual_file_handler.write(line)
    return

def generate_sidebar():
    prepared_sidebar=[]
    with open(index_file, 'r+', encoding='utf-8') as index_file_handler:
        lines = index_file_handler.readlines()
        for line in lines:
            if(line.startswith(starts_title)):
                prepared_sidebar.append({'type':title_key, 'content':line[len(starts_title):]})
            elif(line.startswith(starts_section)):
                prepared_sidebar.append({'type':section_key, 'content':line[len(starts_section):]})
            elif(line.startswith(starts_subsection)):
                prepared_sidebar.append({'type':subsection_key, 'content':line[len(starts_subsection):]})
    with open(sidebar_file, 'w', encoding='utf-8') as sidebar_file_handler:
        for item in prepared_sidebar:
            if(item['type']==title_key):
                sidebar_file_handler.write("<b>"+item['content'].upper()+"</b><br>")
            if(item['type']==section_key):
                sidebar_file_handler.write("&#8227; <b>"+item['content']+"</b><br>")
            if(item['type']==subsection_key):
                sidebar_file_handler.write("&#8227;&#8227; "+item['content']+"<br>")
    return

generate_sidebar()
remove_ext_md()