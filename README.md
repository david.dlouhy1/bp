# Bakalářksá práce - David Dlouhý

Repozitář bakalářské práce studenta Davida Dlouhého - M19000010.

## Struktura
### VM<br>
>Obsahuje informace a soubory týkající se virtuálních strojů.

### Tools<br>
>Obahuje základní a další užitečné a doplňkové nástroje pro projekt.

## Distribuce

### Testovací distribuce
**Identifikátor:** dis_test<br>
**Popis:**<br>*Jedná se jednoduchou distribuci, která je určená pro základní úkon a seznámení se s Yocto.*